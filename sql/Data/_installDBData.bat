@ECHO OFF
TITLE Database Installation Tool
COLOR 0A

:TOP
CLS
ECHO.
ECHO          浜様様様様様様様様様様様様様様様融
ECHO          �                                �
ECHO          �    Welcome to the world DB     �
ECHO          �              for               �
ECHO          �      EnturionEMU 406a/420a      �
ECHO          �        Installation Tool       �
ECHO          �                                �
ECHO          藩様様様様様様様様様様様様様様様夕
ECHO.
ECHO.
ECHO    Please enter your MySQL Info...
ECHO.
SET /p host= MySQL Server Address (e.g. localhost):
ECHO.
SET /p user= MySQL Username: 
SET /p pass= MySQL Password: 
ECHO.
SET /p world_db= World Database:
SET port=3306

FOR %%C IN (.\*.sql) DO (
	ECHO Importing: %%~nxC
	..\dep\mysql\mysql -v --host=%host% --user=%user% --password=%pass% --port=%port% %world_db% < "%%~fC"
	ECHO Successfully imported %%~nxC
)
pause